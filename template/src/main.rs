fn setup_log() {
    use tracing_subscriber::{prelude::*, registry, EnvFilter};

    let registry = registry();

    let filter = EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));
    let stdout_layer = tracing_subscriber::fmt::layer()
        .compact()
        .with_filter(filter);
    let registry = registry.with(stdout_layer);

    registry.init();
}

#[tokio::main]
async fn main() {
    let result = dotenvy::dotenv();
    setup_log();
    if let Err(e) = result {
        tracing::warn!("Failed to load .env file: {}", e);
    }
    if let Err(e) = main_result().await {
        tracing::error!("{}", e);
    }
}

async fn main_result() -> anyhow::Result<()> {
    tracing::info!(
        "Hello from {} v{}!",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION")
    );
    Ok(())
}

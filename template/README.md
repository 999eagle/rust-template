# {{ project-name | title_case }}

{% if license != "none" -%}
## License

Licensed under {% if license == "MIT" -%}
MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT).
{%- elsif license == "Apache-2.0" -%}
Apache License, Version 2.0 ([LICENSE-Apache-2.0](LICENSE-Apache-2.0) or http://www.apache.org/licenses/LICENSE-2.0).
{%- else -%}
either of

- Apache License, Version 2.0 ([LICENSE-Apache-2.0](LICENSE-Apache-2.0) or http://www.apache.org/licenses/LICENSE-2.0)
- MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
{%- endif -%}
{%- endif %}

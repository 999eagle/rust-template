# Rust project template

A simple template for [cargo-generate](https://github.com/cargo-generate/cargo-generate) mainly intended for binary crates. Contains some setup for `tracing`.

## Usage

```
cargo generate https://gitlab.com/999eagle/rust-template
````
